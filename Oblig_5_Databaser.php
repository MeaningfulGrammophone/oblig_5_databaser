<!DOCTYPE html>
<html>
<head>
	<title>oblig_5</title>
	<style type="text/css">
	* {
		font-family: "Arial";
	}
	</style>
</head>
<body>
<h3>oblig_5</h3>
<?php 
	
	class SkierLog {
		protected $doc = null;
		protected $x = null;

		public function __construct() {
			try {
				$this->doc = new DOMDocument();
				if (!$this->doc->load("./SkierLogs.xml")) {
					die("<p style='color:red;'>Error: Can't load XML file.</p>");
				}
            catch (Exception $e) {
                die("<p style='color:red;'>Error: Can't load XML file: " . $e->getMessage() . "</p>");
            }
		}

		public function getSkiers() {
			$array = array();
			$x = $this->x;

			foreach ($x->query("/SkierLogs/Skiers/Skier") as $item) {
				$userName = $item->getAttribute("userName");
				$firstName = $x->query("./FirstName", $item)->item(0)->textContent;
				$lastName = $x->query("./LastName", $item)->item(0)->textContent;
				$yearOfBirth = $x->query("./YearOfBirth", $item)->item(0)->textContent;

				$skier = array(
					"userName"=>$userName,
					"firstName"=>$firstName,
					"lastName"=>$lastName,
					"yearOfBirth"=>$yearOfBirth
				);
				array_push($array, $skier);
			}
			return $array;
		}
	}

	class DBModel  {
		protected $db = null;

		const HOST = "127.0.0.1";
		const DBNAME = "oblig_5";
		const USER = "root";
		const PWD = "";
		const OPTS = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
		const CONNSTRING = "mysql:host=" . self::HOST . ";dbname=" . self::DBNAME;

		public function __construct() {
			try {
				$this->db = new PDO(self::CONNSTRING, self::USER, self::PWD, self::OPTS);
			} catch (Exception $e) {
				die("<p style='color:red;'>Error: Can't connect to database: " . $e->getMessage() . "</p>");
			}
		}

		public function wipe() {
			$stmt = $this->db->prepare("DELETE FROM skier;");
			$stmt->execute();
		}

		public function insertSkier($userName, $firstName, $lastName, $yearOfBirth) {
			$stmt = $this->db->prepare("
				INSERT INTO skier(userName, firstName, lastName, yearOfBirth) 
				VALUES (:userName, :firstName, :lastName, :yearOfBirth)"
			);
			$stmt->bindValue(":userName", $userName);
			$stmt->bindValue(":firstName", $firstName);
			$stmt->bindValue(":lastName", $lastName);
			$stmt->bindValue(":yearOfBirth", $yearOfBirth);
			$stmt->execute();
		}
	}

	$log = new SkierLog();
	$model = new DBModel();
	$model->wipe(); // DEBUG

	$skiers = $log->getSkiers();
	foreach ($skiers as $skier) {
		$model->insertSkier($skier["userName"], $skier["firstName"], $skier["lastName"], (int)$skier["yearOfBirth"]);
	}

?>
</body>
</html>